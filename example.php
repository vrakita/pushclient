<?php

require 'vendor/autoload.php';

$test = new \PushClient\Messaging('my-key');

try {
    $response = $test->setMessage('Hello World')
        ->setData(['type' => 'something', 'event' => 'test', 'url' => null])
        ->setChannel('/topics/global')
        ->setIcon('my-icon.png')
        ->sslOff()
        ->send();
} catch (\Exception $e) {
    $response = $e->getMessage();
}

print_r($response);