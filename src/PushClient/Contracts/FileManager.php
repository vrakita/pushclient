<?php namespace PushClient\Contracts;

interface FileManager {

    public function save($path, $content);

}