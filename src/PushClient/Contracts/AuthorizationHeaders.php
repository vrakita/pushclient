<?php namespace PushClient\Contracts;

interface AuthorizationHeaders {

    /**
     * Make authorization headers
     *
     * @return string
     */
    public function makeAuthorizationHeaders();

    /**
     * Return SSL value from .env
     *
     * @return string
     */
    public function getSSL();

}