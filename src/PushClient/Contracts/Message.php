<?php namespace PushClient\Contracts;

interface Message {

    /**
     * Set message for sending
     *
     * @param string $message
     * @return $this
     */
    public function setMessage($message);

    /**
     * Set title for sending
     *
     * @param string $title
     * @return $this
     */
    public function setTitle($title);

    /**
     * Set channel for sending messages
     *
     * @param string $channel
     * @return $this
     */
    public function setChannel($channel);

    /**
     * Set notification's icon
     *
     * @param string $icon
     * @return $this
     */
    public function setIcon($icon);

    /**
     * Turn off SSL
     *
     * @return $this
     */
    public function sslOff();

    /**
     * Set additional data to send
     *
     * @param array $data
     * @return $this
     */
    public function setData($data);

    /**
     * Set message priority
     *
     * @param string $priority
     * @return $this
     */
    public function setPriority($priority);

    /**
     * Return data array for sending;
     *
     * @return array $data
     */
    public function getData();

    /**
     * Send message
     *
     * @return mixed
     * @throws \Exception
     */
    public function send();
}