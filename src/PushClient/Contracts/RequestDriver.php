<?php namespace PushClient\Contracts;

interface RequestDriver {

	/**
	 * Set url to call
	 *
	 * @param string $url
	 * @return $this
	 */
	public function setUrl($url);

	/**
	 * Return url which is called
	 *
	 * @return mixed
	 */
	public function getUrl();

	/**
	 * Set data for request
	 *
	 * @param array $data
	 * @return $this
	 */
	public function setData(array $data);

	/**
	 * Return data array which is set for request
	 *
	 * @return array
	 */
	public function getData();

	/**
	 * Set headers for request
	 *
	 * @return $this
	 */
	public function sslOff();

	/**
	 * Returns ssl value
	 *
	 * @return int
	 */
	public function getSSL();

	/**
	 * Set headers for request
	 *
	 * @param string $headers
	 * @return $this
	 */
	public function setHeaders($headers);

	/**
	 * Returns headers array which is set for request
	 *
	 * @return array
	 */
	public function getHeaders();

	/**
	 * Return return transfer
	 *
	 * @return bool
	 */
	public function getReturnTransfer();

	/**
	 * Set return transfer
	 *
	 * @param bool $returnTransfer
	 * @return $this
	 */
	public function setReturnTransfer($returnTransfer);

	/**
	 * Set request as POST
	 *
	 * @return $this
	 */
	public function post();

	/**
	 * Set request as GET (default)
	 *
	 * @return $this
	 */
	public function get();

	/**
	 * Return cURL response info
	 *
	 * @return array
	 */
	public function getResponseInfo();

	/**
	 * Return cURL response info
	 *
	 * @return int|null
	 */
	public function getResponseCode();

	/**
	 * Execute cURL call
	 *
	 * @return mixed
	 */
	public function call();

}