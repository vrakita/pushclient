<?php namespace PushClient\Drivers;

use PushClient\Contracts\RequestDriver;

class CurlDriver implements RequestDriver {

    /**
     * cURL handler
     *
     * @var resource
     */
	protected $ch;

    /**
     * URL to be called
     *
     * @var
     */
	protected $url;

    /**
     * Return transfer for cURL
     *
     * @var bool
     */
	protected $returnTransfer = true;

    /**
     * SSL on request
     *
     * @var int
     */
	protected $ssl = 2;

    /**
     * Data to be sent with request
     *
     * @var array
     */
	protected $data = [];

    /**
     * Headers for request
     *
     * @var array
     */
	protected $headers = [];

    /**
     * Flag for POST request
     *
     * @var bool
     */
	protected $isPost = false;

    /**
     * Response info after request is made
     *
     * @var array
     */
	protected $responseInfo = [];


	
	public function __construct() {
	
		$this->ch = curl_init();
	
	}

	/**
	 * Set url to call
	 *
	 * @param string $url
	 * @return $this
	 */
	public function setUrl($url) {
	
		$this->url = $url;
		
		return $this;
	
	}

	/**
	 * Return url which is called
	 *
	 * @return mixed
	 */
	public function getUrl() {
	
		return $this->url;
	
	}

	/**
	 * Set data for request
	 *
	 * @param array $data
	 * @return $this
	 */
	public function setData(array $data) {
	
		$this->data = array_merge_recursive($this->data, $data);
		
		return $this;
	
	}

	/**
	 * Return data array which is set for request
	 *
	 * @return array
	 */
	public function getData() {
	
		return $this->data;
	
	}

	/**
	 * Set headers for request
	 *
	 * @param string $headers
	 * @return $this
	 */
	public function setHeaders($headers) {
	
		$this->headers[] = $headers;
		
		return $this;
	
	}

	/**
	 * Returns headers array which is set for request
	 *
	 * @return array
	 */
	public function getHeaders() {
	
		return $this->headers;
	
	}

    /**
     * Set headers for request
     *
     * @return $this
     */
    public function sslOff() {

        $this->ssl = 0;

        return $this;

    }

    /**
     * Returns ssl value
     *
     * @return int
     */
    public function getSSL() {

        return $this->ssl;

    }

	/**
	 * Return return transfer
	 *
	 * @return bool
	 */
	public function getReturnTransfer() {
	
		return $this->returnTransfer;
	
	}

	/**
	 * Set return transfer
	 *
	 * @param bool $returnTransfer
	 * @return $this
	 */
	public function setReturnTransfer($returnTransfer) {
	
		$this->returnTransfer = (bool) $returnTransfer;
		
		return $this;
	
	}

	/**
	 * Set request as POST
	 *
	 * @return $this
	 */
	public function post() {
	
		$this->isPost = true;
		
		return $this;
	
	}

	/**
	 * Set request as GET (default)
	 *
	 * @return $this
	 */
	public function get() {
	
		$this->isPost = false;
		
		return $this;
	
	}

	/**
	 * Return cURL response info
	 *
	 * @return array
	 */
	public function getResponseInfo() {

		return $this->responseInfo;

	}

	/**
	 * Return cURL response info
	 *
	 * @return int|null
	 */
	public function getResponseCode() {

		return isset($this->responseInfo['http_code']) ? $this->responseInfo['http_code'] : NULL;

	}
	
	protected function dataExists() {
	
		return ! empty($this->data);
	
	}
	
	protected function prepareData() {
	
		if( $this->dataExists()) $this->isPost ? curl_setopt($this->ch, CURLOPT_POSTFIELDS, json_encode($this->data)) : $this->url .= "?" . http_build_query($this->data);
	
	}
	
	
	protected function setOptions() {
	
		curl_setopt($this->ch, CURLOPT_URL, $this->getUrl());
		curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, $this->getSSL());
		curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, $this->getSSL());
		curl_setopt($this->ch, CURLOPT_POST, (bool) $this->isPost);
		curl_setopt($this->ch, CURLOPT_HTTPHEADER, $this->getHeaders());
		curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, FALSE);
	
	}

	/**
	 * Execute cURL call
	 *
	 * @return mixed
	 */
	public function call() {

		$this->prepareData();
		$this->setOptions();
		$response = curl_exec($this->ch);
		$this->responseInfo = curl_getinfo($this->ch);
        $this->data = [];
		return $response;
	
	}

	public function __destruct() {
		curl_close($this->ch);
	}


}