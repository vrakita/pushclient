<?php namespace PushClient;

use PushClient\Drivers\CurlDriver;
use PushClient\Contracts\Message;

class Messaging implements Message {

    /**
     * Driver for making requests
     *
     * @var CurlDriver
     */
    protected $driver;

    /**
     * Google Cloud Messaging key
     *
     * @var $key
     */
    protected $key;

    /**
     * URL for sending messages
     *
     * @var string
     */
    protected $url      = 'https://fcm.googleapis.com/fcm/send';

    /**
     * Channel for sending messages
     *
     * @var
     */
    protected $channel;

    /**
     * SSL on sending
     *
     * @var bool
     */
    protected $ssl      = true;

    /**
     * Notification content
     *
     * @var array $notification
     */
    protected $notification;


    public function __construct($key) {
        $this->key                                      = $key;
        $this->driver                                   = new CurlDriver();

        $this->notification['priority']                 = 'high';
        $this->notification['notification']['title']    = null;
        $this->notification['notification']['icon']     = '';
    }

    /**
     * Set message for sending
     *
     * @param string $message
     * @return $this
     */
    public function setMessage($message) {

        $this->notification['notification']['body'] = $message;

        return $this;

    }

    /**
     * Set message title for sending
     *
     * @param string $title
     * @return $this
     */
    public function setTitle($title) {

        $this->notification['notification']['title'] = $title;

        return $this;

    }

    /**
     * Set notification's icon
     *
     * @param string $icon
     * @return $this
     */
    public function setIcon($icon) {

        $this->notification['notification']['icon'] = (string) $icon;

        return $this;

    }

    /**
     * Set message priority
     *
     * @param string $priority
     * @return $this
     */
    public function setPriority($priority) {

        $this->notification['priority'] = $priority;

        return $this;

    }

    /**
     * Set channel for sending messages
     *
     * @param string $channel
     * @return $this
     */
    public function setChannel($channel) {

        $this->notification['to'] = $channel;

        return $this;

    }

    /**
     * Turn off SSL
     *
     * @return $this
     */
    public function sslOff() {

        $this->ssl = false;

        return $this;

    }

    /**
     * Set additional data to send
     *
     * @param array $data
     * @return $this
     */
    public function setData($data) {

        $this->notification['data'] = $data;

        return $this;

    }

    /**
     * Return data array for sending;
     *
     * @return array $data
     */
    public function getData() {

        return $this->notification;

    }

    /**
     * Send message
     *
     * @return mixed
     * @throws \Exception
     */
    public function send() {

        $this->driver->setHeaders('Content-Type: application/json')
            ->setHeaders('Authorization: key=' . $this->key)
            ->setUrl($this->url)
            ->setData($this->notification)
            ->post();
        if( ! $this->ssl) $this->driver->sslOff();
        $response = $this->driver->call();
        if($this->driver->getResponseCode() !== 200) throw new \Exception($this->driver->getResponseCode());
        return $response;

    }

}